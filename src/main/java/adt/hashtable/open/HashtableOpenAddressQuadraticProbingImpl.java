package adt.hashtable.open;

import adt.hashtable.hashfunction.HashFunctionClosedAddressMethod;
import adt.hashtable.hashfunction.HashFunctionQuadraticProbing;

public class HashtableOpenAddressQuadraticProbingImpl<T extends Storable>
		extends AbstractHashtableOpenAddress<T> {

	public HashtableOpenAddressQuadraticProbingImpl(int size,
			HashFunctionClosedAddressMethod method, int c1, int c2) {
		super(size);
		hashFunction = new HashFunctionQuadraticProbing<T>(size, method, c1, c2);
		this.initiateInternalTable(size);
	}

	@Override
	public void insert(T element) {
		if (isFull()) {
			throw new HashtableOverflowException();
		}
		if (search(element) == null) {
			int probe = 0;
			int hash = hash(element, probe);
			while (probe < table.length && table[hash] != null && !table[hash].equals(deletedElement)) {
				probe++;
				hash = hash(element, probe);
				COLLISIONS++;
			}
			table[hash] = element;
			elements++;
		}	
	}

	@Override
	public void remove(T element) {
		if (!isEmpty()) {
			int index = indexOf(element);
			if (index != -1) {
				table[index] = deletedElement;
				elements--;
			}
		}
	}

	@Override
	public T search(T element) {
		T result = null;
		if (!isEmpty()) {
			int index = indexOf(element);
			if (index != -1) {
				result = (T) table[index];
			}
		}
		return result;
	}

	@Override
	public int indexOf(T element) {
		int index = -1;
		int probe = 0;
		int hash = hash(element, probe);
		while (probe < table.length && table[hash] != null) {
			if (table[hash].equals(element)) {
				index = hash;
				break;
			}
			probe++;
			hash = hash(element, probe);
		}
		return index;
	}

	private int hash(T element, int probe) {
		return ((HashFunctionQuadraticProbing<T>) hashFunction).hash(element, probe);
	}
}
